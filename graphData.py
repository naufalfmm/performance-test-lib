import matplotlib.pyplot as plt
from lib.warning import Warning


# opt = {'title': '', 'legend': {'show':, 'pos':}, 'xAxis': [{'name':, 'type':, 'data':[]}, {'name':, 'type':, 'data':[]}], 'yAxis': [{'name':, 'type':, 'data':[]}, {'name':, 'type':, 'data':[]}], 'series': [{'name': '', 'data': [], 'x':, 'y':, 'marker': '', 'color': ''}, {'name': '', 'data': [], 'x':, 'y':, 'marker': '', 'color': ''}]}
# required --> series
# Rule:
# 1. If one of the series set x(y) above the length of x(y)Axis or when undefined x(y)Axis, x(y) value still be taken and the rule of x(y)Axis will be set default
# 2. Title will be used as png filename. If the title is undefined or empty, the graph cannot be converted into image file.
# 3. If legend is undefined, legend will not be shown
class GraphData:
    def __init__(self, opt):
        try:
            self.createObject(opt)
        except BaseException as exc:
            raise Exception(exc)

    def createObject(self, opt):
        try:
            self.checkRequired(key=["series"], opt=opt)

            self.opt = opt

            if 'title' not in opt:
                return [
                    True,
                    Warning("Graph will not be converted into image file")
                ]
            elif opt['title'] == '':
                return [
                    True,
                    Warning("Graph will not be converted into image file")
                ]

            return [True]
        except BaseException as exc:
            raise Exception(exc)

    def checkRequired(self, key, opt={}):
        for i in key:
            if i not in opt:
                raise Exception("'" + i + "' key is not defined")

    def getTitle(self):
        try:
            return self.opt['title']
        except KeyError:
            return None

    def setTitle(self, title):
        self.opt['title'] = title

    def getSeries(self, th='all'):
        try:
            if th == 'all':
                return self.opt['series']
            else:
                return self.opt['series'][th]
        except (KeyError, IndexError):
            return None

    # seri = {'name': '', 'data': [], 'x':, 'y':}
    # req --> data
    # x and y value is started by 0
    def setSeries(self, th, seri):
        try:
            self.checkRequired(key=["data"], opt=seri)
            if th > len(self.opt['series']):
                self.opt['series'].append(seri)
            else:
                self.opt['series'][th] = seri
        except BaseException as exc:
            raise Exception(exc)

    def getYAxis(self, id='all'):
        try:
            if id == 'all':
                return self.opt['yAxis']
            else:
                return self.opt['yAxis'][id]
        except (KeyError, IndexError):
            return None

    # yAxis = {'name':, type:, data: []}
    # req --> data
    def setYAxis(self, yAxis, id=None):
        try:
            self.checkRequired(key=['data'], opt=yAxis)
            yAxises = self.getYAxis()

            if yAxises is None:
                self.opt['yAxis'] = [yAxis]
            elif id > len(yAxises):
                self.opt['yAxis'].append(yAxis)
            else:
                self.opt['yAxis'][id] = yAxis
        except BaseException as exc:
            raise Exception(exc)

    def getXAxis(self, id='all'):
        try:
            if id == 'all':
                return self.opt['xAxis']
            else:
                return self.opt['xAxis'][id]
        except (KeyError, IndexError):
            return None

    # xAxis = {'name':, type:, data: []}
    # req --> data
    def setXAxis(self, xAxis, id=None):
        try:
            self.checkRequired(key=['data'], opt=xAxis)
            xAxises = self.getXAxis()

            if xAxises is None:
                self.opt['xAxis'] = [xAxis]
            elif id > len(xAxises):
                self.opt['xAxis'].append(xAxis)
            else:
                self.opt['xAxis'][id] = xAxis
        except BaseException as exc:
            raise Exception(exc)

    # adding the series data
    def __add__(self, data=[], id=0):
        selfSeries = self.getSeries(th=id)
        if selfSeries is None:
            raise Exception('Series id is undefined')
        else:
            selfSeries['data'] += data
            self.setSeries(id, selfSeries)

        return GraphData(opt=self.opt)

    def draw(self):
        try:
            yAxises = self.getYAxis()
            xAxises = self.getXAxis()
            series = self.getSeries()

            xIdExist = []
            yIdExist = []

            xExist = {}
            yExist = {}

            _, ax1y1 = plt.subplots()
            y = series[0]["data"]
            x = []

            if 'x' in series[0]:
                if len(xAxises) < series[0]['x'] + 1:
                    for i in range(len(y)):
                        x.append(i)
                    x = {'data': x}
                else:
                    x = xAxises[series[0]['x']]

                xIdExist.append(series[0]['x'])
                xExist[series[0]['x']] = x
            else:
                if len(xAxises) == 0:
                    for i in range(len(y)):
                        x.append(i)
                    x = {'data': x}
                else:
                    x = xAxises[0]

                xIdExist.append(0)
                xExist[0] = x

            if 'y' in series[0]:
                yIdExist.append(series[0]['y'])
                if len(yAxises) >= series[0]['y'] + 1:
                    yExist[series[0]['y']] = yAxises[series[0]['y']]
                else:
                    yExist[series[0]['y']] = {}
            else:
                yIdExist.append(0)
                if len(yAxises) < 1:
                    yExist[0] = {}
                else:
                    yExist[0] = yAxises[0]

            if 'color' not in series[0]:
                ax1y1.plot(x, y)
            else:
                ax1y1.plot(x, y, c=series[0]['color'])

            # xIdExist.append(series[0]['x'])
            # yIdExist.append(series[0]['y'])

            for seri in series[1:]:
                y = seri['data']
                x = []
                pass

            # if type(self.y) is not list:
            #     _, ax1 = plt.subplots()

            #     ax2 = ax1.twinx()
            #     ax1.plot(self.x, self.y['y1'], c=self.opt['y1']['color'])
            #     ax2.plot(self.x, self.y['y2'], c=self.opt['y2']['color'])

            #     ax1.set_xlabel(self.opt['x']['name'])
            #     ax1.set_ylabel(self.opt['y1']['name'],
            #                    c=self.opt['y1']['color'])
            #     ax2.set_ylabel(self.opt['y2']['name'],
            #                    c=self.opt['y2']['color'])
            # else:
            #     plt.plot(self.x, self.y, c=self.opt['y']['color'])
            #     plt.xlabel(self.opt['x']['name'])
            #     plt.ylabel(self.opt['y']['name'])

            title = self.getTitle()
            if title != '' and title != None:
                plt.savefig(title + ".png")
                return [True, title + ".png"]
            else:
                return [True, "Image filename is empty or undefined"]
        except BaseException as exc:
            return [False, str(exc)]


# data = [{}, {}, {}]
# {x: [], y: []}
# name --> name of graph image file (without extension)
# class GraphData:
#     def __init__(self, opt, name='', x=[], y=[]):
#         self.name = name
#         self.x = x
#         self.y = y
#         self.opt = opt

#     def __add__(self, other):
#         newName = self.name
#         if other.name != '': newName = newName + "_" + other.name

#         newX = self.x + other.x
#         newY = self.y + other.y

#         return GraphData(name=newName, x=newX, y=newY, opt=self.opt)

#     def joinY(self, opt, newY=[]):
#         if type(self.y) is list:
#             self.y = {'y1': self.y, 'y2': newY}
#             self.opt['y1'] = self.opt['y']
#             self.opt['y2'] = opt
#             del self.opt['y']
#         else:
#             raise Exception("Number of Y axis has reached the maximum number")

#     def printData(self):
#         print(self.name, self.x, self.y)

#     def draw(self):
#         try:
#             if type(self.y) is not list:
#                 _, ax1 = plt.subplots()

#                 ax2 = ax1.twinx()
#                 ax1.plot(self.x, self.y['y1'], c=self.opt['y1']['color'])
#                 ax2.plot(self.x, self.y['y2'], c=self.opt['y2']['color'])

#                 ax1.set_xlabel(self.opt['x']['name'])
#                 ax1.set_ylabel(self.opt['y1']['name'],
#                                c=self.opt['y1']['color'])
#                 ax2.set_ylabel(self.opt['y2']['name'],
#                                c=self.opt['y2']['color'])
#             else:
#                 plt.plot(self.x, self.y, c=self.opt['y']['color'])
#                 plt.xlabel(self.opt['x']['name'])
#                 plt.ylabel(self.opt['y']['name'])

#             plt.savefig(self.name + ".png")
#             return [True, self.name + ".png"]
#         except BaseException as exc:
#             return [False, str(exc)]
