from unittest import TestCase, main
from unittest.mock import Mock, patch
import sys
from nodeflux.performanceTest.baselineProfile import BaselineProfile


class BaselineProfileTest(TestCase):
    def setUp(self):
        self.mock = Mock()
        self.baseline = BaselineProfile(self.mock)

    def tearDown(self):
        self.mock = None
        self.baseline = None

    def test_init(self):
        self.assertTrue(self.baseline)
        self.mock.assert_not_called()

    def test_errorMessage_with_empty_error_message(self):
        res = self.baseline._BaselineProfile__errorMessage([])
        self.assertEqual(res, '')

    def test_errorMessage_with_some_error_message(self):
        res = self.baseline._BaselineProfile__errorMessage(
            [[1, 'Error message'], [2, 'Error message']])
        self.assertEqual(res, "1: Error message\n2: Error message\n")

    def test_timeFocus_with_achievedUsers_is_None(self):
        with self.assertRaises(BaseException) as exc:
            self.baseline.timeFocus()
        self.assertEqual(str(exc.exception),
                         "Profiling never run or returned error")

    def test_timeFocus_with_achievedUsers_is_any_value(self):
        self.baseline._BaselineProfile__achievedUsers = 25
        res = self.baseline.timeFocus()
        self.assertEqual(res, 25)

    def test_usersFocus_with_achievedTime_is_None(self):
        with self.assertRaises(BaseException) as exc:
            self.baseline.usersFocus()
        self.assertEqual(str(exc.exception),
                         "Profiling never run or returned error")

    def test_usersFocus_with_achievedTime_is_any_value(self):
        self.baseline._BaselineProfile__achievedTime = 0.01
        res = self.baseline.usersFocus()
        self.assertEqual(res, 0.01)

    @patch('nodeflux.performanceTest.baselineProfile.StressTest')
    def test_profilingProcess_with_no_error(self, stressTestMock):
        stressTestMock.return_value.getResult.return_value = [
            (True, 1, 1, 0.001, 0.001, 0.001, [], []),
            (True, 2, 2, 0.001, 0.001, 0.001, [], [])
        ]
        res = self.baseline._BaselineProfile__profilingProcess()
        self.assertListEqual(res, [2, 1])

    @patch('nodeflux.performanceTest.baselineProfile.StressTest')
    def test_profilingProcess_with_warning_time(self, stressTestMock):
        self.baseline = BaselineProfile(self.mock, timeTarget=1, usersTarget=3)
        stressTestMock.return_value.getResult.return_value = [
            (True, 1, 1, 0.001, 0.001, 0.001, [], []),
            (True, 2, 2, 0.001, 0.001, 0.001, [(1, 3)], []),
            (True, 3, 3, 0.001, 0.001, 0.001, [], [])
        ]
        res = self.baseline._BaselineProfile__profilingProcess()
        self.assertListEqual(res, [1, 3])


if __name__ == "__main__":
    main()
