from unittest import TestCase, main
from nodeflux.performanceTest.lib.math import median, stdDeviation


class StdDeviationTest(TestCase):
    def test_len_equal_to_zero(self):
        with self.assertRaises(BaseException) as exc:
            stdDeviation([], 0)
        self.assertEqual(str(exc.exception), "There is no single data")

    def test_len_equal_to_one(self):
        result = stdDeviation([1], 1)
        self.assertEqual(result, 0)

    def test_len_more_than_one(self):
        result = stdDeviation([1, 2, 3], 2)
        self.assertEqual(result, 1.0)


class MedianTest(TestCase):
    def test_len_equal_to_zero(self):
        with self.assertRaises(BaseException) as exc:
            median([])
        self.assertEqual(str(exc.exception), "There is no single data")

    def test_len_equal_to_one(self):
        result = median([1])
        self.assertEqual(result, 1)

    def test_len_is_odd(self):
        result = median([1, 2, 3, 4, 5])
        self.assertEqual(result, 3)

    def test_len_is_even(self):
        result = median([1, 2, 3, 4, 5, 6])
        self.assertEqual(result, 4)


if __name__ == '__main__':
    main()