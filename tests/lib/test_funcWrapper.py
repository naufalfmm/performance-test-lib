from unittest.mock import Mock
from unittest import TestCase, main
from nodeflux.performanceTest.lib.funcWrapper import funcWrapper


class FuncWrapperTest(TestCase):
    def test_no_argument(self):
        mock = Mock(return_value=None)
        res = funcWrapper(mock)
        self.assertListEqual(res, [True, None])
        mock.assert_called_once_with()

    def test_with_some_arguments(self):
        mock = Mock(return_value=None)
        res = funcWrapper(mock, 1, 2, 3)
        self.assertListEqual(res, [True, None])
        mock.assert_called_once_with(1, 2, 3)

    def test_return_exception(self):
        mock = Mock(side_effect=BaseException("Some Exception"))
        res = funcWrapper(mock, 1, 2, 3)
        self.assertListEqual(res, [False, "Some Exception"])
        mock.assert_called_once_with(1, 2, 3)


if __name__ == '__main__':
    main()
