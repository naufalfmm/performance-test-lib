from unittest import TestCase, main
from nodeflux.performanceTest.lib.warning import Warning


class WarningTest(TestCase):
    def test_with_some_string(self):
        warn = Warning("Example warning message")
        self.assertEqual(str(warn),
                         "\033[33m Warning: Example warning message\033[0m")

    def test_with_no_message_argument(self):
        warn = Warning()
        self.assertEqual(str(warn), "\033[33m Warning: \033[0m")


if __name__ == "__main__":
    main()
