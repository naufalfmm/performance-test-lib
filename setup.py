# from distutils.core import setup
from setuptools import setup

REQUIRED = ['matplotlib']
NAME = "performanceTestLib"
DESCRIPTION = "Library of Performance Testing"
URL = "https://gitlab.com/nodefluxio/qa/performance-test-lib"
EMAIL = "muhammadnaufalfm@gmail.com"
AUTHOR = "Naufal Farras"
VERSION = "0.1.5"

setup(name=NAME,
      description=DESCRIPTION,
      packages=[
          'nodeflux', 'nodeflux.performanceTest',
          'nodeflux.performanceTest.lib'
      ],
      url=URL,
      install_requires=REQUIRED,
      author=AUTHOR,
      author_email=EMAIL,
      version=VERSION)
