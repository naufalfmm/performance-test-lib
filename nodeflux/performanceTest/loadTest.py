from threading import Thread, Lock
from .lib.funcWrapper import funcWrapper
from .lib.math import stdDeviation
from time import sleep, time
import matplotlib.pyplot as plt


# result = {'Ramp Up': [(), (), (), ...], 'Peak': [(), (), (), ...], 'Slide Down': [(), (), (), ...]}
# (status, successUser, totalUser, timeTotal, meanTime, stdevTime, failedMessage)
# status returns error while one of the users on the group returns false
# failedMessage = [id, errMessage], [], []
class LoadTest:
    def __init__(self,
                 func,
                 *args,
                 userPeak=100,
                 rampUpTime=100,
                 peakLoadCount=100,
                 peakTime=100):
        # self.__func = funcWrapper(func, *args)
        self.__func = func
        self.__args = args
        self.__userPeak = userPeak  # number of users at peak condition
        self.__rampUpTime = rampUpTime  # total time of users number increase/decrease (s)
        self.__peakLoadCount = peakLoadCount  # count of load at peak condition
        self.__peakTime = peakTime  # total time at peak condition (s)
        self.__resultData = {}

    def getResult(self):
        return self.__resultData

    def drawGraph(self, folderLoc, name):
        x = [i for i in range(self.__userPeak * 2 + self.__peakLoadCount)]
        _, ay1 = plt.subplots()
        ay2 = ay1.twinx()

        y1 = []
        for i in self.__resultData.keys():
            for j in self.__resultData[i]:
                y1.append(j[2])

        y2 = []
        for i in self.__resultData.keys():
            for j in self.__resultData[i]:
                y2.append(j[4])

        y3 = []
        for i in self.__resultData.keys():
            for j in self.__resultData[i]:
                y3.append(j[5])

        ay1.plot(x, y1, c='black', linestyle='--', label='Total of Users')
        ay2.plot(x, y2, c='green', label='Mean of Time (s)')
        ay2.plot(x, y3, c='blue', label='Standard Deviation of Time (s)')
        ay1.set_ylim(bottom=0)
        ay2.set_ylim(bottom=0)

        h1, l1 = ay1.get_legend_handles_labels()
        h2, l2 = ay2.get_legend_handles_labels()
        plt.legend(
            h1 + h2,
            l1 + l2,
            loc='upper center',
            bbox_to_anchor=(0.55, -0.05),
            ncol=3,
            # fancybox=True,
            # shadow=True,
        )
        plt.title(name)
        plt.savefig(folderLoc + name + ".png", bbox_inches='tight')

    def __resultConc(self, resultData):
        try:
            resultData.sort()

            successCall = 0
            timeTotal = 0
            failedCall = []
            time = []

            boolRes = True

            for res in resultData:
                if not res[1]:
                    boolRes = False
                    failedCall.append([res[0], res[len(res) - 1]])
                    if len(res) > 3:
                        timeTotal += res[2]
                        time.append(res[2])
                else:
                    successCall += 1
                    timeTotal += res[2]
                    time.append(res[2])

            meanTime = timeTotal / len(resultData)
            stdevTime = stdDeviation(time, meanTime)

            return (boolRes, successCall, len(resultData), timeTotal, meanTime,
                    stdevTime, failedCall)
        except BaseException as exc:
            raise Exception("__resultConc error:", exc)

    def __funcRun(self, id, resData, resLock):
        try:
            start = time()
            # ret = self.__func
            ret = funcWrapper(self.__func, *self.__args)
            end = time()

            resLock.acquire()
            resData.append((id, ret[0], (end - start), ret[1]))
            resLock.release()
        except BaseException as exc:
            resLock.acquire()
            resData.append((id, False, str(exc)))
            resLock.release()

    def __rampingUp(self):
        resultLock = Lock()
        resultData = []

        rampUpSleep = self.__rampUpTime / self.__userPeak

        threads = []
        lastThread = 0

        try:
            for i in range(self.__userPeak):
                resultData.append([])
                for j in range(i + 1):
                    threads.append(
                        Thread(target=self.__funcRun,
                               args=(
                                   j,
                                   resultData[i],
                                   resultLock,
                               )))

                    threads[j + lastThread].start()

                for k in range(i + 1):
                    threads[k + lastThread].join()

                lastThread += (i + 1)

                resultData[i] = self.__resultConc(resultData[i])

                sleep(rampUpSleep)

            return resultData
        except BaseException as exc:
            for thr in threads:
                thr.join()

            raise Exception("Ramp up error:", exc)

    def __peakLoad(self):
        resultLock = Lock()
        resultData = []

        peakLoadSleep = self.__peakTime / self.__peakLoadCount

        threads = []
        lastThread = 0

        try:
            for i in range(self.__peakLoadCount):
                resultData.append([])
                for j in range(self.__userPeak):
                    threads.append(
                        Thread(target=self.__funcRun,
                               args=(
                                   j,
                                   resultData[i],
                                   resultLock,
                               )))

                    threads[j + lastThread].start()

                for k in range(i + 1):
                    threads[k + lastThread].join()

                lastThread += self.__userPeak

                resultData[i] = self.__resultConc(resultData[i])

                sleep(peakLoadSleep)

            return resultData
        except BaseException as exc:
            for thr in threads:
                thr.join()

            raise Exception("Peak load error:", exc)

    def __slidingDown(self):
        resultLock = Lock()
        resultData = []

        slidingDownSleep = self.__rampUpTime / self.__userPeak

        threads = []
        lastThread = 0

        try:
            for i in range(self.__userPeak, 0, -1):
                resultData.append([])
                for j in range(i):
                    threads.append(
                        Thread(target=self.__funcRun,
                               args=(
                                   j,
                                   resultData[(i - self.__userPeak) * -1],
                                   resultLock,
                               )))

                    threads[j + lastThread].start()

                for k in range(i):
                    threads[k + lastThread].join()

                lastThread += i

                resultData[(i - self.__userPeak) * -1] = self.__resultConc(
                    resultData[(i - self.__userPeak) * -1])

                sleep(slidingDownSleep)

            return resultData
        except BaseException as exc:
            for thr in threads:
                thr.join()

            raise Exception("Slide down error:", exc)

    def run(self):
        try:
            self.resultGraph = {}
            self.__resultData['Ramp Up'] = self.__rampingUp()
            self.__resultData['Peak'] = self.__peakLoad()
            self.__resultData['Slide Down'] = self.__slidingDown()

            return [True]
        except BaseException as exc:
            return [False, str(exc)]
