from threading import Thread, Lock
from .lib.funcWrapper import funcWrapper
from .lib.math import stdDeviation
from time import sleep, time
import signal


# result = [(), (), (), ...]
# (status, successCall, totalCall, timeTotal, meanTime, stdevTime, warning, failedMessage)
# status returns error while one of the users on the group returns false
# warning = [(id, time), (id, time), ...] --> if user in the group need time more than timeThreshold
# failedMessage = [[id, errMessage], [id, errMessage], ...]
# the processes of Stress Test stop when
# 1. The time mean of the group > timeThreshold
# 2. The group returns false --> get, at least, one error
# 3. The process has reached the maxUsers (done)
class StressTest:
    def __init__(self,
                 func,
                 *args,
                 startUsers=1,
                 maxUsers=2000,
                 step=1,
                 sleepTime=1,
                 timeThreshold=3):
        self.__checkData(func,
                         *args,
                         startUsers=startUsers,
                         maxUsers=maxUsers,
                         step=step,
                         sleepTime=sleepTime,
                         timeThreshold=timeThreshold)

    def __checkData(self, func, *args, startUsers, maxUsers, step, sleepTime,
                    timeThreshold):
        if startUsers < 0:
            raise Exception("Start users must be more or equal than 0")

        if maxUsers < startUsers:
            raise Exception("Max users must be more or equal than start users")

        if step < 1:
            raise Exception("Step must be more or equal than 1")

        self.__func = func
        self.__args = args
        self.__startUsers = startUsers
        self.__maxUsers = maxUsers
        self.__step = step
        self.__sleepTime = sleepTime
        self.__timeThreshold = timeThreshold
        self.__resultData = []

    def getResult(self):
        return self.__resultData

    def __resultConc(self, resultData):
        try:
            resultData.sort()

            successCall = 0
            timeTotal = 0
            warningCall = []
            failedCall = []
            time = []

            boolRes = True

            for res in resultData:
                if not res[1]:
                    boolRes = False
                    failedCall.append([res[0], res[len(res) - 1]])
                    if len(res) > 3:
                        timeTotal += res[2]
                        time.append(res[2])

                        if res[2] > self.__timeThreshold:
                            warningCall.append([res[0], res[2]])
                else:
                    successCall += 1
                    timeTotal += res[2]
                    time.append(res[2])

                    if res[2] > self.__timeThreshold:
                        warningCall.append([res[0], res[2]])

            meanTime = timeTotal / len(resultData)
            stdevTime = stdDeviation(time, meanTime)

            if meanTime > self.__timeThreshold:
                boolRes = False

            return (boolRes, successCall, len(resultData), timeTotal, meanTime,
                    stdevTime, warningCall, failedCall)
        except BaseException as exc:
            raise Exception("__resultConc error:", exc)

    def __funcRun(self, id, resData, resLock):
        try:
            start = time()
            ret = funcWrapper(self.__func, *self.__args)
            end = time()

            resLock.acquire()
            resData.append((id, ret[0], (end - start), ret[1]))
            resLock.release()
        except BaseException as exc:
            resLock.acquire()
            resData.append((id, False, str(exc)))
            resLock.release()

    def __loopFunc(self, userNumb):
        resultData = []
        resultLock = Lock()

        threads = []

        try:
            for i in range(userNumb):
                threads.append(
                    Thread(target=self.__funcRun,
                           args=(
                               i,
                               resultData,
                               resultLock,
                           )))

                threads[i].start()

            for thr in threads:
                thr.join()

            resultData = self.__resultConc(resultData)

            return resultData
        except BaseException as exc:
            for thr in threads:
                thr.join()

            raise Exception("__loopFunc", userNumb, "error:", exc)

    def run(self):
        print(self.__func)
        resultData = []

        threads = []

        try:
            for i in range(self.__startUsers, self.__maxUsers + 1,
                           self.__step):
                resultData.append(self.__loopFunc(i))
                sleep(self.__sleepTime)

            self.__resultData = resultData

            return [True]
        except BaseException as exc:
            for thr in threads:
                thr.join()

            return [False, str(exc)]
