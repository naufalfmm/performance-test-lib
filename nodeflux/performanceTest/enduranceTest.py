from threading import Thread, Lock, Event
from .lib.funcWrapper import funcWrapper
from .lib.math import stdDeviation
from time import sleep, time


# result = [(), (), (), ...]
# (status, successCall, totalCall, timeTotal, meanTime, stdevTime, failedMessage)
# status returns error while one of the users on the group returns false
# failedMessage = [id, errMessage]
class EnduranceTest:
    def __init__(self,
                 func,
                 *args,
                 userNumb=100,
                 callTime=3600,
                 intervalTime=0):
        self.__userNumb = userNumb
        self.__callTime = callTime
        self.__intervalTime = intervalTime
        # self.__func = funcWrapper(func, *args)
        self.__func = func
        self.__args = args
        self.__resultData = []
        self.__stopEvent = Event()

    def getResult(self):
        return self.__resultData

    def drawGraph(self):
        pass

    def __resultConc(self, resultData):
        try:
            resultData.sort()

            k = 0
            for i in resultData:
                if i[1] or (not i[1] and type(i[2]).__name__ != 'str'):
                    successCall = 0
                    timeTotal = 0
                    failedCall = []
                    time = []

                    for j in i[2]:
                        if not j[0]:
                            failedCall.append([j[1], j[3]])
                        else:
                            successCall += 1

                        timeTotal += j[2]
                        time.append(j[2])

                    meanTime = timeTotal / len(i[2])
                    stdevTime = stdDeviation(time, meanTime)

                    resultData[k] = (i[1], successCall, len(i[2]), timeTotal,
                                     meanTime, stdevTime, failedCall)
                else:
                    resultData[k] = i

                k += 1

            return resultData
        except BaseException as exc:
            print("__resultConc exc", str(exc))
            raise Exception(exc)

    def __loopFunc(self, id, resData, resLock):
        res = []
        boolRes = True
        i = 0
        try:
            while not self.__stopEvent.is_set():
                start = time()
                # ret = self.__func
                ret = funcWrapper(self.__func, *self.__args)
                end = time()

                if not ret[0]:
                    boolRes = False
                    res.append([False, i, (end - start), ret[1]])
                else:
                    res.append([True, i, (end - start)])

                i += 1

                sleep(self.__intervalTime)

            resLock.acquire()
            resData.append((id, boolRes, res))
            resLock.release()
        except BaseException as exc:
            resLock.acquire()
            resData.append((id, False, str(exc)))
            resLock.release()

    def run(self):
        resultData = []
        resultLock = Lock()

        threads = []

        try:
            for i in range(self.__userNumb):
                threads.append(
                    Thread(target=self.__loopFunc,
                           args=(
                               i,
                               resultData,
                               resultLock,
                           )))
                threads[i].start()

            sleep(self.__callTime)
            self.__stopEvent.set()

            for thr in threads:
                thr.join()

            self.__resultData = self.__resultConc(resultData)

            return [True]
        except BaseException as exc:
            for thr in threads:
                thr.join()

            return [False, str(exc)]
