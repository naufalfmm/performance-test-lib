from threading import Thread, Lock
from .lib.funcWrapper import funcWrapper
from .lib.math import stdDeviation
from time import sleep, time


# result = [(), (), (), ...]
# (status, successCall, totalCall, timeTotal, meanTime, stdevTime, failedMessage)
# status returns error while one of the users on the group returns false
# failedMessage = [id, errMessage]
class DDosTest:
    def __init__(self, func, *args, userNumber=1, callNumber=1, sleepCall=0):
        self.__userNumber = userNumber
        self.__callNumber = callNumber
        self.__sleepCall = sleepCall
        self.__func = func
        self.__args = args
        self.__resultData = []

    def getResult(self):
        return self.__resultData

    def __resultConc(self, resultData):
        try:
            successCall = 0
            timeTotal = 0
            failedCall = []
            time = []

            for res in resultData[-1]:
                if not res[0]:
                    failedCall.append([res[1], res[-1]])
                    if len(res) > 3:
                        timeTotal += res[2]
                        time.append(res[2])
                else:
                    successCall += 1
                    timeTotal += res[2]
                    time.append(res[2])

            meanTime = timeTotal / len(resultData)
            stdevTime = stdDeviation(time, meanTime)

            return (resultData[1], successCall, len(resultData[-1]), timeTotal,
                    meanTime, stdevTime, failedCall)
        except BaseException as exc:
            raise Exception("__resultConc error:", exc)

    def __loopFunc(self, id, resData, resLock):
        res = []
        boolRes = True

        try:
            for i in range(self.__callNumber):
                start = time()
                ret = funcWrapper(self.__func, *self.__args)
                end = time()

                if not ret[0]:
                    boolRes = False
                    res.append([ret[0], i, (end - start), ret[1]])
                else:
                    res.append([True, i, (end - start)])

                sleep(self.__sleepCall)

            resLock.acquire()
            resData.append(self.__resultConc((id, boolRes, res)))
            resLock.release()
        except BaseException as exc:
            resLock.acquire()
            resData.append((id, False, str(exc)))
            resLock.release()

    def run(self):
        resultData = []
        resultLock = Lock()

        threads = []

        try:
            for i in range(self.__userNumber):
                threads.append(
                    Thread(target=self.__loopFunc,
                           args=(
                               i,
                               resultData,
                               resultLock,
                           )))
                threads[i].start()

            for thr in threads:
                thr.join()

            self.__resultData = resultData

            return [True]
        except BaseException as exc:
            for thr in threads:
                thr.join()

            return [False, str(exc)]
