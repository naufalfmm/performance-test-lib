class Warning:
    def __init__(self, message=''):
        self.message = message

    def __str__(self):
        return '\033[33m Warning: ' + self.message + '\033[0m'
