from math import pow


def stdDeviation(data, mean):
    if len(data) == 0: raise Exception("There is no single data")

    total = 0
    for i in data:
        total += pow((i - mean), 2)

    if len(data) > 1: return (total / (len(data) - 1))
    else: return (total / len(data))


def median(data):
    if len(data) == 0:
        raise Exception("There is no single data")
    # elif len(data) == 1:
    #     return data[0]
    data.sort()
    return data[(len(data) // 2)]
