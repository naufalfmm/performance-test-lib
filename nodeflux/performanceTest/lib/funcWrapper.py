def funcWrapper(func, *args):
    try:
        ret = func(*args)
        return [True, ret]
    except BaseException as exc:
        return [False, str(exc)]
