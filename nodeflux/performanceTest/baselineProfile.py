from .stressTest import StressTest
from .lib.funcWrapper import funcWrapper
from .lib.math import median


class BaselineProfile:
    def __init__(self,
                 func,
                 *args,
                 timeTarget=1,
                 usersTarget=1,
                 profilingTimes=1,
                 method='min'):
        self.__func = func
        self.__args = args
        self.__timeTarget = timeTarget  # requirely-achieved target of load time
        self.__usersTarget = usersTarget  # requirely-achieved target of simultaneous users
        self.__profilingTimes = profilingTimes
        self.__method = method  # min, max, median
        self.__achievedUsers = None
        self.__achievedTime = None

    # it will return the maximum simultaneous users of targetted time
    def timeFocus(self):
        if self.__achievedUsers is None:
            raise Exception("Profiling never run or returned error")

        return self.__achievedUsers

    # it will return the needed time of targetted number of users
    def usersFocus(self):
        if self.__achievedTime is None:
            raise Exception("Profiling never run or returned error")

        return self.__achievedTime

    def __errorMessage(self, errArr):
        errMessage = ''
        for err in errArr:
            errMessage += (str(err[0]) + ": " + err[1] + "\n")

        return errMessage

    def __profilingProcess(self):
        stress = StressTest(self.__func,
                            *self.__args,
                            maxUsers=self.__usersTarget,
                            timeThreshold=self.__timeTarget)

        stress.run()

        res = stress.getResult()

        maxUser = 0  # in target of time
        maxTime = 0  # in number of simultaneous users
        for j in res:
            if len(j[7]) > 0:
                errMessage = self.__errorMessage(j[7])
                raise Exception(errMessage)
            elif len(j[6]) > 0:
                break

            maxUser = j[2]

        if len(res[self.__usersTarget - 1][7]) > 0:
            errMessage = self.__errorMessage(res[self.__usersTarget - 1][7])
            raise Exception(errMessage)
        elif len(res[self.__usersTarget - 1][6]) > 0:
            max = 0
            for j in res[self.__usersTarget - 1][6]:
                if j[1] > max:
                    max = j[1]
            maxTime = max
        else:
            maxTime = self.__timeTarget

        return [maxUser, maxTime]

    def profiling(self):
        if self.__method == 'median':
            achievedUsers = []  # achieved users is in the target of time
            achievedTime = []  # needed time is in the target of users

            for _ in range(self.__profilingTimes):
                res = self.__profilingProcess()
                achievedUsers.append(res[0])
                achievedTime.append(res[1])

            self.__achievedUsers = median(achievedUsers)
            self.__achievedTime = median(achievedTime)
        elif self.__method == 'min':
            achievedUsers = self.__usersTarget
            achievedTime = self.__timeTarget

            for _ in range(self.__profilingTimes):
                res = self.__profilingProcess()

                if res[0] < achievedUsers:
                    achievedUsers = res[0]

                if res[1] < achievedTime:
                    achievedTime = res[1]

            self.__achievedTime = achievedTime
            self.__achievedUsers = achievedUsers
        else:
            achievedUsers = 0
            achievedTime = 0

            for _ in range(self.__profilingTimes):
                res = self.__profilingProcess()

                if res[0] > achievedUsers:
                    achievedUsers = res[0]

                if res[1] > achievedTime:
                    achievedTime = res[1]

            self.__achievedTime = achievedTime
            self.__achievedUsers = achievedUsers
